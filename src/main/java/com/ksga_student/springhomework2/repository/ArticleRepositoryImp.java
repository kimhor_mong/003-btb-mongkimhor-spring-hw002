package com.ksga_student.springhomework2.repository;

import com.ksga_student.springhomework2.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class ArticleRepositoryImp implements ArticleRepository {
    List<Article> articles = new ArrayList<>();

    public ArticleRepositoryImp(){
        articles.add(new Article(1, "title 1", "description 1 ", "0e7845f2-36ca-4927-ba97-0ffa8d9bf0f8.png"));
        articles.add(new Article(2, "title 2", "description 2", "1c459245-d014-4877-8c33-8be1f43164af.png"));
        articles.add(new Article(3, "title 3", "description 3", "1cd9abce-a9f7-4666-9679-45f0cb7b0b6b.png"));
    }

    @Override
    public List<Article> findAll() {
        return articles;
    }

    @Override
    public Article findById(int id) {
        return articles.get(id - 1);
    }

    @Override
    public boolean add(Article article) {
        articles.add(article);
        return true;
    }

    @Override
    public boolean edit(Article updatedArticle) {
       Article oldArticle = findById(updatedArticle.getId());
       oldArticle.setTitle(updatedArticle.getTitle());
       oldArticle.setDescription(updatedArticle.getDescription());

       if(!oldArticle.getImage().equals(updatedArticle.getImage())){
           oldArticle.setImage(updatedArticle.getImage());
       }

        return true;
    }

    @Override
    public boolean remove(int id) {
        articles.remove(id);
        return true;
    }
}
